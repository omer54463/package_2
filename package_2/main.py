from package_1.package_1 import Package1
from package_2.package_2 import Package2


def main() -> None:
    package_1 = Package1()
    package_1.method()

    package_2 = Package2()
    package_2.method()


if __name__ == "__main__":
    main()
