from package_1.package_1 import Package1


class Package2(Package1):
    def method(self) -> None:
        super().method()
        print("Package2::package_2")
